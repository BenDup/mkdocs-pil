Voici 3 autres scrpits assez simples :<br>
⚠️ L'image réelle étant grande et détaillée (HD), l'exécution du script peut prendre 5 à 10 secondes !

## Script no 4

{{ IDE('test_pil2') }}

<div id="textfield">L'image originale</div>
<div id="original_image"></div>
<div id="textfield">L'image convertie</div>
<div id="new_image"></div>


## Script no 5

Dans l'IDE du dessus, il faut remplacer la transfo de l'image (ET QUE LA TRANSFO, sans l'intro ni l'enregistrement) par :

```python
#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            #traitement de la couleur du pixel
            if pixel[0]>127:
                r=255
            else:
                r=0
            if pixel[1]>127:
                v=255
            else:
                v=0
            if pixel[2]>127:
                b=255
            else:
                b=0
            p=(r,v,b)
            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)
```

## Script no 6

Dans l'IDE du dessus, il faut remplacer la transfo de l'image (ET QUE LA TRANSFO, sans l'intro ni l'enregistrement) par :

```python
#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            p=(pixel[0],pixel[1],pixel[2])

            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((c-1-x,y),p)
```
