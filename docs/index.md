# Traitement d'image avec Python

_Version actuelle de pyodide : {{ config.plugins.pyodide_macros.version }}_
<br>
**L'objectif est de découvrir différents traitements automatisés d'image sous python.** <br>
L'image de référence est ici : [- Château de Chaumont-sur-Loire - ](https://bendup.forge.apps.education.fr/mkdocs-pil/chateau-chaumont.jpg){:target="_blank" }
(à enregistrer par clic droit...) <br>
L’objectif n’étant pas de faire en soi du traitement d’images, auquel cas on se contenterait d’utiliser un logiciel dont
c’est la finalité.
On va utiliser des programmes réalisés sous python et permettant de réaliser différents traitements AFIN d’analyser
le  code  POUR  découvrir  la  méthode  de  traitement  (et  donc  pourquoi  et  comment  ça  fonctionne,  par  exemple
lorsque vous appliquez un filtre photo sur Instagram 📸).<br>

## Principe général

Comme d'hab dans chaque prog il faudra **à chaque fois** quelques éléments de base : importer les fonctions de bibliothèque `PIL`, ouvrir une image, faire les modifs, enregistrer/afficher le résultat.

J'affiche là l'image qu'on va utiliser (uploadée dans un sous-dossier images) réduite à 20% :
![Chateau Chaumont](chateau-chaumont.jpg){ width=20% }

TRAVAIL A FAIRE : <br>
1) Essayer chaque programme sur l’image proposée.<br>
2) Exprimer le résultat de ce qui est obtenu.<br>
3) Expliquer à partir du code du programme proposé comment il agit pour parvenir au résultat obtenu.<br>
CR à remettre sous le nom **« 2dX NOM_P pypaint2 »**.

Let's go, on va la traficoter dans tous les sens ! Voici déjà 3 scrpits assez simples :<br>
⚠️ L'image réelle étant grande et détaillée (HD), l'exécution du script peut prendre 5 à 10 secondes !

## Script no 1

{{ IDE('test_pil') }}

<div id="textfield">L'image originale</div>
<div id="original_image"></div>
<div id="textfield">L'image convertie</div>
<div id="new_image"></div>


## Script no 2

Dans l'IDE du dessus, il faut remplacer la transfo de l'image (ET QUE LA TRANSFO, sans l'intro ni l'enregistrement) par :

```python
#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            #traitement de la couleur du pixel
            p=(0,pixel[1],0)
            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)
```


## Script no 3

Dans l'IDE du dessus, il faut remplacer la transfo de l'image (ET QUE LA TRANSFO, sans l'intro ni l'enregistrement) par :

```python
#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            #traitement de la couleur du pixel
            p=(255-pixel[0],255-pixel[1],255-pixel[2])
            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)
```
