# --- PYODIDE:env --- #

#image_jpg = "https://bendup.forge.apps.education.fr/mkdocs-pil/chateau-chaumont.jpg"

# Récupère sur le serveur l'image dans le dossier du `index.md` 
# correspondant à la page en cours, puis crée un fichier du même 
# nom dans l'environnement de pyodide :
#await copy_from_server(image_jpg)
import micropip
import pyodide
import io
import asyncio
from js import fetch
from pyodide.http import pyfetch

url_fichier = "chateau-chaumont.jpg"
reponse = await pyfetch(url_fichier)
data = await reponse.bytes()
bytes_list = bytearray(data)
my_bytes = io.BytesIO(bytes_list)

async def copy_from_server(
    src: str,
    dest: str=".",
    name: str="",
):

    """
    Récupère le fichier à l'adresse `src` (absolue ou relative au dossier de la 
    page en cours), et crée son équivalent sur le disque virtuel de pyodide à
    l'adresse `dest/nom_de_fichier`.
    `nom_de_fichier` est soit `name`, ou si le 3e argument n'est pas utilisé, 
    c'est le nom de fichier à la fin de `src` qui est utilisé.

    Exemple :

        await copy_from_server("../other/img.jpg", "work/black_white")

        => Crée le fichier `work/black_white/img.jpg` sur le disque virtuel.
    """
# --- PYODIDE:ignore --- #
"""
Code inspiré très fortement de https://community.anaconda.cloud/t/image-uploading-and-manipulation-demo/23586/5
"""
# --- PYODIDE:code --- #

from PIL import Image
from js import document, console, Uint8Array, window, File

await copy_from_server("../docs/chateau-chaumont.jpg", "mkdocs-pil/3script2")
im=Image.open('chateau-chaumont.jpg')

#on créé une fonction qui effectue la conversion de l'image
def tt(image):
    #on récupère les dimensions de l'image
    (c,l)=image.size
    #on créé une image d'arrivée de même dimensions
    imagearrivee=Image.new('RGB',(c,l))
    #on va parcourir toutes les colonnes
    for x in range (c):
        #pour chaque colonne on va parcourir chaque ligne
        for y in range (l):
            #on récupère le triplet RGB de chaque pixel
            pixel=image.getpixel((x,y))
            #traitement de la couleur du pixel
            gris=int((pixel[0]+pixel[1]+pixel[2])/3)
            p=(gris,gris,gris)
            #affectation de la couleur du pixel de l'image selon la valeur calculée
            imagearrivee.putpixel((x,y),p)


    # On enregistre dans un flux
    stream = io.BytesIO()
    imagearrivee.save(stream, format='PNG')
    processed_image_file = File.new([Uint8Array.new(stream.getvalue())], "new_image_file.png", {type: "image/png"})
    new_image = document.createElement('img')
    new_image.src = window.URL.createObjectURL(processed_image_file)
    document.getElementById("new_image").appendChild(new_image)

    original_image_file = File.new([Uint8Array.new(my_bytes.getvalue())], "original_image_file.jpg", {type: "image/jpeg"})
    original_image = document.createElement('img')
    original_image.classList.add("w-auto")
    original_image.src = window.URL.createObjectURL(original_image_file)
    document.getElementById("original_image").appendChild(original_image)

#on lance la fonction tt() créée sur notre image
tt(im)
